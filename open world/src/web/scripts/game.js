var frame_time = 60/1000;

 var game_core = function(game_instance){
    this.instance = game_instance;

    this.world = {
        width: 700,
        height: 700
    };

    this.input = {
        width: 300,
        height: 30
    };

    this.dx = [1, 0, -1, 0];
    this.dy = [0, 1, 0, -1];

    this.cell = {
        width: 100,
        height: 100
    };

    this.keyboard = new THREEx.KeyboardState();

    this.client_connect_to_server();
 }

 game_core.prototype.client_initialize = function() {
    var mx = this.world.width / 2;
    var my = this.world.height / 2;


    this.username = new CanvasText(
        document.getElementById('viewport'),{
        x:mx - this.input.width / 2,
        y:my - this.input.height,
        width:this.input.width,
        placeholder:'Enter your username...',
        }
    );
    this.join_button = new CanvasSubmit(
        document.getElementById('viewport'),{
        x:mx - this.input.width / 2,
        y:my + 10,
        width:this.input.width,
        placeholder:'Join the Game',
        onSubmit:this.join_request
    });

//     var input = new CanvasInput({
//       canvas: document.getElementById('viewport'),
//       fontSize: 18,
//       fontFamily: 'Arial',
//       fontColor: '#212121',
//       fontWeight: 'bold',
//       width: 300,
//       padding: 8,
//       borderWidth: 1,
//       borderColor: '#000',
//       borderRadius: 3,
//       boxShadow: '1px 1px 0px #fff',
//       innerShadow: '0px 0px 5px rgba(0, 0, 0, 0.5)',
//       placeHolder: 'Please enter your username'
//     });
//     input.on('keydown', function(e) {
//
//     });

     var cnt = 0;
     this.ground_images = new Array();
     for (var i = 0; i < 3; i++) {
       var image = new Image();
       image.onload = function () {
         cnt++;
	   };
       image.src = "ground" + i + ".jpg";
	   this.ground_images[i] = image;
     }

     this.arrow_image = new Image();
     this.arrow_image.src = "arrow.jpg";
 }

function draw4Arrows() {
  game.ctx.globalAlpha = 0.5;

  var angle = 0;
  for (var i = 0; i < 4; i++) {
    if (game.field[(game.field.length - 1) / 2 + game.dx[i]][(game.field[0].length - 1) / 2 + game.dy[i]] != 2) {
      game.ctx.save();
      game.ctx.translate(game.world.width / 2 + game.dx[i] * game.cell.width, game.world.height / 2 + game.dy[i] * game.cell.height);
      game.ctx.rotate(angle);
      game.ctx.drawImage(game.arrow_image, -game.cell.width / 2, -game.cell.height / 2);
      game.ctx.restore();
    }
    angle += Math.PI / 2;
  }
  game.ctx.globalAlpha = 1;
}

 game_core.prototype.join_request = function() {
    var jsonObject = {userName: game.username.value};
    game.socket.emit('join_request', jsonObject);
 }

game_core.prototype.client_ondisconnect = function() {
}

game_core.prototype.client_onmoveresponse_recieved = function(data) {
  if (data.success) {
    return;
  }game.moving = true;
  var layer = data.layer;
  var speed = data.speed;
  var direction = data.direction;

  var updated_field = [];
  for (var i = 0; i < game.field.length + 2; i++) {
    updated_field[i] = [];
    for (var j = 0; j < game.field[0].length + 2; j++) {
      updated_field[i][j] = -1;
    }
  }

  for (var i = 0; i < game.field.length; i++) {
    for (var j = 0; j < game.field[i].length; j++) {
      updated_field[i + 1][j + 1] = game.field[i][j];
    }
  }

  var f = direction >= 2 ? 0 : (game.field.length + game.field[0].length + 1 - layer.length);

  for (var i = 0; i < layer.length; i++) {
    if (direction % 2 == 0)
     updated_field[f][i + 1] = layer[i];
    else
      updated_field[i + 1][f] = layer[i];
  }

  shift(0, game.cell.width / 5, speed, updated_field, direction);
}

function shift(now, add, speed, updated_field, direction) {
  for (var i = 0; i < updated_field.length; i++) {
    for (var j = 0; j < updated_field[i].length; j++) {
      if (updated_field[i][j] != -1)
        game.ctx.drawImage(game.ground_images[updated_field[i][j]], (i - 1) * 100 - now * game.dx[direction], (j - 1) * 100 - now * game.dy[direction]);
    }
  }

  if (now == game.cell.width) {
    field = [];
    for (var i = 0; i < game.field.length; i++) {
      field[i] = [];
      for (var j = 0; j < game.field[i].length; j++) {
        field[i][j] = updated_field[i + game.dx[direction] + 1][j + game.dy[direction] + 1];
      }
    }
    game.field = field;
    draw4Arrows();
    game.moving = false;
    return;
  }

  setTimeout(function() {
    shift(now + add, add, speed, updated_field, direction);
  }, speed);
}

game_core.prototype.client_onstart_recieved = function(data) {
  if (!data.accepted) {
    this.ctx.fillStyle = "red";
    this.ctx.font = "10px Arial";
    this.ctx.fillText("You should use another username.", this.world.width / 2 - 75, this.world.height / 2 + 10 + this.input.height);
    return;
  }

  this.ctx.clearRect(0, 0, this.world.width, this.world.height);
  this.username.destroy();
  this.join_button.destroy();

  this.field = [];
  for (var i = 0; i < data.field.length; i++) {
    this.field[i] = [];
	for (var j = 0; j < data.field[i].length; j++) {
/*	  this.field[i][j] = new Image();
	  this.field[i][j].onload = function() {
	  	game.ctx.drawImage(this, 20, 20);
	  }
	  this.field[i][j].src = "ground" + data.field[i][j] + ".jpg";*/
	  game.ctx.drawImage(this.ground_images[data.field[i][j]], i * 100, j * 100);
    }
  }
  this.field = data.field;

  this.viewport.addEventListener(
    "mousedown",
     function(event) {
       if (game.moving) return;
       for (var d = 0; d < 4; d++) {
         var xc = game.world.width / 2 + game.dx[d] * game.cell.width;
         var yc = game.world.height / 2 + game.dy[d] * game.cell.height;

         if (event.pageX > xc - game.cell.width / 2 && event.pageX < xc + game.cell.width / 2)
           if (event.pageY > yc - game.cell.height / 2 && event.pageY < yc + game.cell.height / 2)
             game.socket.emit('move_request', {'direction': d});
       }
     },
     false
  );

  draw4Arrows();
}

game_core.prototype.client_onnewvote_recieved = function() {
  alert("New question!");
}

 game_core.prototype.client_connect_to_server = function() {

         //Store a local reference to our connection to the server
         this.socket = io.connect('http://localhost:9092');

         //Sent when we are disconnected (network, server down, etc)
         this.socket.on('disconnect', this.client_ondisconnect.bind(this));
         //Response on the move in such direction
         this.socket.on('move_response', this.client_onmoveresponse_recieved.bind(this));
         //Message with initial position
         this.socket.on('start', this.client_onstart_recieved.bind(this));
         //Message with new vote acknowledge up
         this.socket.on('new_vote', this.client_onnewvote_recieved.bind(this));
 }; //game_core.client_connect_to_server