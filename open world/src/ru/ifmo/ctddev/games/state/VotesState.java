package ru.ifmo.ctddev.games.state;

import java.util.Map;
import java.util.HashMap;

/**
 * Created by Aksenov239 on 30.08.2014.
 */
public class VotesState {
    private static Map<Long, Vote> votes;
    private static Map<Long, Vote> archived;
    private static long id = 1;

    // TODO: There obviously should be synchronization

    public VotesState() {
        votes = new HashMap<Long, Vote>();
        archived = new HashMap<Long, Vote>();
    }

    public void addVote(int moneyToPut, int[] money, int voteLimit, String question, String... options) {
        Vote vote = new Vote(id, moneyToPut, money, voteLimit, question, options);
        votes.put(id++, vote);
    }

    public void addVote(int levelLimit, String question, String... options) {
        Vote vote = new Vote(id++, levelLimit, question, options);
        votes.put(id++, vote);
    }

    public static Vote[] getVotes() {
        Vote[] result = new Vote[votes.size()];
        int l = 0;
        for (Vote vote : votes.values()) {
            result[l++] = vote;
        }
        return result;
    }

    public static boolean vote(PlayerState state, long id, int option, int amount) {
        Map<Long, Integer> player_votes = state.getVotes();
        if (!player_votes.containsKey(id)) {
            player_votes.put(id, 0);
        }
        int already = player_votes.get(id);
        Vote vote = votes.get(id);
        if (vote.getVoteLimit() <= already || vote.getLevelLimit() > state.getLevel()) {
            return false;
        }
        if (vote.getType() == 1) {
            int spend = vote.getMoney(amount);
            if (spend > state.getMoney()) {
                return false;
            }
        }

        player_votes.put(id, already++);
        return vote.vote(option, amount);
    }

    public static void setArchived(Vote v) {
        votes.remove(v.getId());
        archived.put(v.getId(), v);
    }

    public boolean active(Long id) {
        return votes.containsKey(id);
    }

}
