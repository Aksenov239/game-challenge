package ru.ifmo.ctddev.games.messages;

import ru.ifmo.ctddev.games.state.Vote;
import java.util.Map;

/**
 * Created by Aksenov239 on 28.08.2014.
 */
public class VotesInformationResponseMessage {
    private Vote[] votes;
    private Map<Long, Integer> voted;

    public VotesInformationResponseMessage() {
    }

    public VotesInformationResponseMessage(Vote[] votes, Map<Long, Integer> voted) {
        super();
        this.votes = votes;
        this.voted = voted;
    }

    public Vote[] getVotes() {
        return votes;
    }

    public void setVotes(Vote[] votes) {
        this.votes = votes;
    }

    public Map<Long, Integer> getVoted() {
        return voted;
    }

    public void setVoted(Map<Long, Integer> voted) {
        this.voted = voted;
    }
}
