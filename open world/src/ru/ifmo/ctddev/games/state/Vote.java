package ru.ifmo.ctddev.games.state;

/**
 * Created by Aksenov239 on 30.08.2014.
 */
public class Vote {
    private long id;
    private String question;
    private String[] options;
    private int levelLimit = -1;
    private int moneyToPut;
    private int[] money;
    private int voteLimit;
    private boolean active = true;
    private int type = 0;

    private int[] votes;
    private int result = -1;

    public Vote(long id, int moneyToPut, int[] money, int voteLimit, String question, String... options) {
        this.id = id;
        this.moneyToPut = moneyToPut;
        this.money = money;
        this.voteLimit = voteLimit;
        this.question = question;
        this.options = options;
        this.votes = new int[options.length];
        type = 0;
    }

    public Vote(long id, int levelLimit, String question, String... options) {
        this.id = id;
        this.levelLimit = levelLimit;
        this.voteLimit = -1;
        this.question = question;
        this.options = options;
        this.votes = new int[options.length];
        type = 1;
    }

    public long getId() {
        return id;
    }

    public void stop() {
        VotesState.setArchived(this);
        this.active = false;
    }

    public boolean vote(int option, int amount) {
        if (!this.active || votes.length <= option || option < 0)
            return false;
        if (type == 0) {
            if (amount >= money.length || amount < 0)
                return false;
            votes[option] += money[amount];
            if (votes[option] >= moneyToPut) {
                stop();
            }
        } else {
            votes[option]++;
        }
        return true;
    }

    public int[] getResult() {
        return votes;
    }

    public int getWinner() {
        if (result != -1)
            return result;
        int mi = 0;
        for (int i = 0; i < options.length; i++) {
            if (votes[i] > mi)
                mi = i;
        }
        return mi;
    }

    public int getLevelLimit() {
        return levelLimit;
    }

    public int getVoteLimit() {
        return voteLimit;
    }
    
    public int getMoney(int amount) {
        return money[amount];
    }
    
     public int getType( ) {
        return type;
    }
}
